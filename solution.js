{
  init: function(elevators, floors) {
    var NONE = "none", UP = "up", DOWN = "down"; //Direction constants
    var FULL = 0.7; //Load factor is calculated by weights so it's innacurate
    var TIME_TO_DISTANCE_FACTOR = 3; //Multiplier for the formula to rate the calls
    var calls = {}; //Keep track of calls made with all the necessary associated info

    function getCallKey(floorNum, direction) {
      return floorNum + "-" + direction;
    }

    function getCall(floorNum, direction) {
      return calls[getCallKey(floorNum, direction)]
    }

    function addCall(floorNum, direction) {
      calls[getCallKey(floorNum, direction)] = {
        floorNum: floorNum,
        direction: direction,
        requestTime: _.now()
      }
    }

    function removeCall(floorNum, direction) {
      detachCall(floorNum, direction);
      delete calls[getCallKey(floorNum, direction)];
    }

    function detachCall(floorNum, direction) {
      var call = getCall(floorNum, direction);

      if(call) {
        var elevator = call.elevator;

        if(elevator) {
          //Not need to stop elevator explicitly. If no pressed floors and no other destination (destinationQueue empty) it will stop.
          if(!isFloorPressed(elevator, floorNum)) {
            removeDestination(elevator, floorNum);
          }

          elevator.calls[floorNum] = null;

          if(elevator.mainCall === call) {
            elevator.mainCall = null;
          }
        }
      }
    }

    function attachCall(floorNum, direction, elevator) {
      var call = getCall(floorNum, direction);

      if(call) {
        elevator.calls[floorNum] = call;
        call.elevator = elevator;
      }
    }

    function assignCall(floorNum, direction, elevator) {
      detachCall(floorNum, direction);
      attachCall(floorNum, direction, elevator);
    }

    function addDestination(elevator, floorNum, forceNow) {
      elevator.goToFloor(floorNum, forceNow);
    }

    function removeDestination(elevator, floorNum) {
      elevator.destinationQueue = _.without(elevator.destinationQueue, floorNum);
      elevator.checkDestinationQueue();
    }

    function isFloorPressed(elevator, floorNum) {
      return _.contains(elevator.getPressedFloors(), floorNum);
    }

    function isNotFull(elevator) {
      return elevator.loadFactor() < FULL;
    }

    function getDirection(elevator) {
      //If the elevator has places to go, the next scheduled floor dictates the direction
      if(!_.isEmpty(elevator.destinationQueue)) {
        return elevator.destinationQueue[0] > elevator.currentFloor() ? UP : DOWN;
      }

      //Otherwise if the elevator is attending a call, that dictates the direction
      if(elevator.calls[elevator.currentFloor()]) {
        return elevator.calls[elevator.currentFloor()].direction;
      }

      //Otherwise check for calls in the current floor to decide a direction
      var call = getBestCallToServe(elevator, true);
      return call.direction;
    }

    //Unless we are already stopping in floorNum
      //then can't avoid passengers going in according to indicators
      //which could delay us if they "hijack" the elevator (we are attending a call that is for the opposite direction and they go past the floor where the call was made)
    //only stop to get passengers if there is a call that
      //goes in the same direction and
      //if we are attending a call, that call is also for the same direction (otherwise we could be allowing new passengers to "hijack" the elevator)
    function suitableCallExists(elevator, floorNum, direction) {
      var suitable = !elevator.mainCall || elevator.mainCall.direction === direction;
      return suitable && calls[getCallKey(floorNum, direction)];
    }

    function setIndicators(elevator, intendedDirection, addedDestination) {
      var direction = intendedDirection;
      var destination = addedDestination || elevator.currentFloor();

      if(!_.isEmpty(elevator.destinationQueue)) {
        destination = elevator.destinationQueue[0];
      }

      if(elevator.currentFloor() != destination) {
        direction = destination > elevator.currentFloor() ? UP : DOWN;
      }

      elevator.goingUpIndicator(direction === UP);
      elevator.goingDownIndicator(direction === DOWN);
    }

    //Could tweak or add other heuristics like if another elevator will serve it faster (in theory, sounds like a rabbit hole)
    function getScore(elevator, call, currentTime) {
      var secondsWaiting = (currentTime - call.requestTime)/1000;
      var distanceToCall = Math.abs(elevator.currentFloor() - call.floorNum) + 1;

      return TIME_TO_DISTANCE_FACTOR*secondsWaiting/distanceToCall;
    }

    function getBestCallToServe(elevator, sameFloorOnly) {
      //This should be ok, as doesn't count as a move and probably gets "idle" event retriggered
      //Could go to floor 0, as the most people seem to appear there, but I like it more like this for now
      var bestCall = {
        floorNum: elevator.currentFloor(),
        direction: NONE
      };

      var bestScore = 0;
      var keys = Object.keys(calls);

      for (var k = 0; k < keys.length; k++) {
        var call = calls[keys[k]];

        if(sameFloorOnly && call.floorNum !== elevator.currentFloor()) {
          continue;
        }

        var currentTime = _.now();
        var score = getScore(elevator, call, currentTime);
        if(score > bestScore && (!call.elevator || score > getScore(call.elevator, call, currentTime))) {
          bestScore = score;
          bestCall = call;
        }
      }

      return bestCall;
    }

    //Init elevators logic
    _.each(elevators, function(elevator) {
      elevator.calls = [];

      elevator.on("idle", function() {
        var call = getBestCallToServe(elevator);
        addDestination(elevator, call.floorNum);
        setIndicators(elevator, call.direction, call.floorNum);
        assignCall(call.floorNum, call.direction, elevator);
        elevator.mainCall = call;
      });

      elevator.on("floor_button_pressed", function(floorNum) {
        //Prioritize destinations from inside vs calls. This should allow handling "hijackings" (see suitableCallExists function) better
        //Don't worry about ordering as we will stop on any pressed floors as we pass by (see below)
        addDestination(elevator, floorNum, true);
      });

      elevator.on("passing_floor", function(floorNum, direction) {
        //Added this to check the hijacking case, but it seems it never happens...
        var oppositeDirection = direction === UP ? DOWN : UP;
        if(isFloorPressed(elevator, floorNum) && calls[getCallKey(floorNum, direction)] && elevator.mainCall && elevator.mainCall.direction === oppositeDirection) {
          console.log("HIJACKED");
        }

        //Considering only pressed floors should help dealing with "hijacking" (see suitableCallExists function) better as we won't stop for a call we are serving if we just pass by
        //We should get to the calls in the destination queue eventually anyway
        if(isFloorPressed(elevator, floorNum)) {
          addDestination(elevator, floorNum, true);
        }

        if(isNotFull(elevator) && suitableCallExists(elevator, floorNum, direction)) {
          addDestination(elevator, floorNum, true);
          assignCall(floorNum, direction, elevator);
        }
      });

      elevator.on("stopped_at_floor ", function(floorNum) {
        removeDestination(elevator, floorNum);

        var direction = getDirection(elevator);
        setIndicators(elevator, direction);
        //The elevator should never be completely full if we stop on a floor, so we assume we serve the corresponding call
        //If we are wrong or not everyone goes in, passengers will press the button again
        //The only problem is that if someone was waiting for long and doesn't go in, we will lose that metric
        removeCall(floorNum, direction);
      });
    });

    //Init floors logic
    _.each(floors, function(floor) {
      floor.on("up_button_pressed", function() {
        addCall(floor.floorNum(), UP);
      });

      floor.on("down_button_pressed", function() {
        addCall(floor.floorNum(), DOWN);
      });
    });
  },

  update: function(dt, elevators, floors) {
    //We normally don't need to do anything here
  }
}
